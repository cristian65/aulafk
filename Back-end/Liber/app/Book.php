<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    public function create(Request $request){
        $this->name = $request->name;
        $this->category = $request->category ;
        $this->price = $request->price;
        $this->resume = $request->resume;
        $this->condition = $request->condition;
        $this->save();
    }

    public function update(Request $request, $id)
    {
        if($request->name){
            $this->name = $request->name;
        }
        if($request->category){
            $this->category = $request->category;
        }
        if($request->price){
            $this->price = $request->price;
        }
        if($request->resume){
            $this->resume = $request->resume;
        }
        if($request->condition){
            $this->condition = $request->condition;
        }
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
