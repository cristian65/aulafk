<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function create(Request $request){
        $this->text = $request->text;
        $this->user_id = $request->user_id;
        $this->save();
    }

    public function updateBook(Request $request, $id)
    {
        if($request->text){
            $this->text = $request->text;
        }
        $this->save();
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
